module Assert = struct
  type t

  type done_cb = unit -> unit

  external async : unit -> done_cb = "async" [@@bs.send.pipe: t]

  external ok : bool -> unit = "ok" [@@bs.send.pipe: t]

  external not_ok : bool -> unit = "notOk" [@@bs.send.pipe: t]

  external equal : 'a -> 'a -> unit = "equal" [@@bs.send.pipe: t]

  external not_equal : 'a -> 'a -> unit = "notEqual" [@@bs.send.pipe: t]

  external deep_equal : 'a -> 'a -> unit = "deepEqual" [@@bs.send.pipe: t]

  external not_deep_equal : 'a -> 'a -> unit = "notDeepEqual" [@@bs.send.pipe: t]

  module Msg = struct
    external ok : bool -> string -> unit = "ok" [@@bs.send.pipe: t]

    external not_ok : bool -> string -> unit = "notOk" [@@bs.send.pipe: t]

    external equal : 'a -> 'a -> string -> unit = "equal" [@@bs.send.pipe: t]

    external not_equal : 'a -> 'a -> string -> unit = "notEqual" [@@bs.send.pipe: t]

    external deep_equal : 'a -> 'a -> string -> unit = "deepEqual" [@@bs.send.pipe: t]

    external not_deep_equal : 'a -> 'a -> string -> unit = "notDeepEqual" [@@bs.send.pipe: t]
  end
end

external _test : string -> (Assert.t -> unit) -> unit = "test" [@@bs.val] [@@bs.scope "QUnit"]

external _suite : string -> (unit -> unit) -> unit = "module" [@@bs.val] [@@bs.scope "QUnit"]

type test

type suite = string

module Unsafe = struct
  type fn = unit -> unit

  external to_test : (unit -> unit) -> test = "%identity"

  external from_test : test -> fn = "%identity"
end

let run_test (test : test) = Unsafe.from_test test ()

let test (name : string) (fn : Assert.t -> unit) : test = Unsafe.to_test @@ fun () -> _test name fn

let run (suites : (suite * test list) list) =
  suites
  |> List.iter (fun (suite_name, tests) ->
         _suite suite_name (fun () -> tests |> List.iter run_test))

module TestSync = struct
  open Qunit.Assert

  type some_obj = { foo : string }

  let test_equal is =
    is |> equal 123 123;
    is |> not_equal 123 456

  let test_deep_equal is =
    is |> deep_equal { foo = "bar" } { foo = "bar" };
    is |> not_deep_equal { foo = "bar" } { foo = "baz" }

  let test_ok is =
    is |> ok (true || false);
    is |> not_ok (true && false)

  let tests =
    [
      Qunit.test "test equal" test_equal;
      Qunit.test "test deep equal" test_deep_equal;
      Qunit.test "test ok" test_ok;
    ]
end

module TestAsync = struct
  open Qunit.Assert

  let test_async is =
    let done_cb = is |> async () in
    is |> Msg.ok true "true before";
    Js.Global.setTimeout
      (fun () ->
        is |> Msg.ok true "true during";
        done_cb ())
      300
    |> ignore;
    is |> Msg.ok true "true after"

  let test_async2 is =
    let done_cb = is |> async () in
    is |> Msg.ok true "true before";
    Js.Global.setTimeout
      (fun () ->
        is |> Msg.ok true "true during";
        done_cb ())
      200
    |> ignore;
    is |> Msg.ok true "true after"

  let tests = [Qunit.test "test async" test_async; Qunit.test "test async 2" test_async2]
end

let () = Qunit.run ["Test Sync", TestSync.tests; "Test Async", TestAsync.tests]

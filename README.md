# bs-qunit

Qunit bindings for bucklescript


## Install

```
yarn add -D bs-qunit
```


## Example

See `test/`


## License

```
See LICENSE
```

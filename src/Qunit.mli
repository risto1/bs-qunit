module Assert : sig
  type t

  type done_cb = unit -> unit

  val async : unit -> t -> done_cb

  val ok : bool -> t -> unit

  val not_ok : bool -> t -> unit

  val equal : 'a -> 'a -> t -> unit

  val not_equal : 'a -> 'a -> t -> unit

  val deep_equal : 'a -> 'a -> t -> unit

  val not_deep_equal : 'a -> 'a -> t -> unit

  module Msg : sig
    val ok : bool -> string -> t -> unit

    val not_ok : bool -> string -> t -> unit

    val equal : 'a -> 'a -> string -> t -> unit

    val not_equal : 'a -> 'a -> string -> t -> unit

    val deep_equal : 'a -> 'a -> string -> t -> unit

    val not_deep_equal : 'a -> 'a -> string -> t -> unit
  end
end

type test

type suite = string

val test : string -> (Assert.t -> unit) -> test

val run : (suite * test list) list -> unit
